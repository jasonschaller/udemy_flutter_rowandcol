import 'package:flutter/material.dart';

void main() {
  runApp(new application());
}

class application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Title',
      home: new Scaffold(
        appBar: new AppBar(title: new Text('Row & Column'),),
          body: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            new Text('This'),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text('This'),
                new Text('is'),
                new Text('column'),
              ],
            ),
            new Text('row'),
          ],
        ),
      )
    );
  }
}

